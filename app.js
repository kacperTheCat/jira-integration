const axios = require('axios');
const fs = require('fs');

async function main() {
  try {
    // Retrieve a list of components
    const componentsResponse = await axios.get('https://herocoders.atlassian.net/rest/api/3/project/SP/components');
    const components = componentsResponse.data;

    // Retrieve a list of issues
    const issuesResponse = await axios.get('https://herocoders.atlassian.net/rest/api/3/search?jql=project=SP');
    const issues = issuesResponse.data.issues;

    const componentsWithoutLead = [];

    // Iterate over the components
    for (const component of components) {
      // Check if the component lead field is empty
      if (!component.lead) {
        // Filter the issues for this component
        const componentIssues = issues.filter(issue => issue.fields.components.find(comp => comp.name === component.name));
        componentsWithoutLead.push({
          componentName: component.name,
          issueCount: componentIssues.length
        });
      }
    }

    let result = 'Issues without component lead: \n\n';
    for (const componentId in componentsWithoutLead) {
      const component = componentsWithoutLead[componentId];
      result += `Component: ${component.componentName}\n`;
      result += `Issue Count: ${component.issueCount}\n`;
      result += '-------------------------\n';
    }

    // Save the result to a text file
    fs.writeFileSync('output.txt', result);

    console.log('Result saved to output.txt');
    // Where's the catch? 😅
  } catch (error) {
    console.error('Error:', error.message);
  }
}

main();